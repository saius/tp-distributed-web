## Description

This project was build using [Jekyll](https://jekyllrb.com/) for fast web
development and [PureCSS](https://purecss.io/), a simple styling framework.

The layout of the website is based on [Responsive Side Menu](https://purecss.io/layouts/)
PureCSS's template.

We also use [vanillajs-datepicker](https://github.com/mymth/vanillajs-datepicker) plugin for date pickers,
[select-pure](https://github.com/dudyn5ky1/select-pure) for searchable dropdown widgets and
browser's built-in [LocalStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
to store registered users data.

## Develop

To modify and build this website you need to have Jekyll installed.
You can read the [official installation guide](https://jekyllrb.com/docs/installation/).

Once having Jekyll and `bundle` installed you must install the project's dependencies locally:
```
bundle config set --local path 'vendor/bundle'
bundle install
```

And finally serve the site on http://127.0.0.1:4000/ with:

`bundle exec jekyll serve`

## Deploy

Deployment configuration is as simple as seen in `.gitlab-ci.yml`.

In `_config.yml` under the variable `baseurl` you can find the configured subpath for the site.
If you host the site on an url like myserver.com/thissite/* you'll have to set `baseurl` to `thissite/`, otherwise leave it blank `""`.
This can also be set at runtime/CI with de `--baseurl` option of the build command.

To statically serve the website just copy the contents of `_site` into the server's root public folder.

If you need to rebuild `_site` run:

`bundle exec jekyll build`

## Amazon endpoints and credentials

You can quickly find endpoint urls doing:  
  
`rgrep amazonaws assets -n`

from within the project's root folder.

These yields the following files & lines: 

- `assets/js/search.js`: lines 122, 155 and 343
- `assets/js/payment.js`: line 85

Dynamo's credentials are at the top of `assets/js/aws.js`, variables `aws_region` and `aws_identity_pool_id`.
