var selectpureOrigin, selectpureDestination;

window.addEventListener("load", function() {
  // https://mymth.github.io/vanillajs-datepicker/#/options?id=datepicker-options
  var datepickerOptions = {autohide: true};

  //https://mymth.github.io/vanillajs-datepicker/#/?id=using-from-browser
  var elem = document.querySelector('input[name="start-date"]');
  var datepicker = new Datepicker(elem, datepickerOptions);

  var elem2 = document.querySelector('input[name="end-date"]');
  var datepicker2 = new Datepicker(elem2, datepickerOptions);

  var elem3 = document.querySelector("#origin-select");
  // iata_codes is generated statically in html/jekyll
  selectpureOrigin = new SelectPure(elem3, {
    options: iata_codes,
    autocomplete: true
    //placeholder: "-Please select-"
  });

  var elem4 = document.querySelector("#destination-select");
  // iata_codes is generated statically in html/jekyll
  selectpureDestination = new SelectPure(elem4, {
    options: iata_codes,
    autocomplete: true
    //placeholder: "-Please select-"
  });

  if (!localStorage.loggedin) {
    document.getElementById("login-warning").innerHTML =
      "You are not logged in. You won't be able to reserve a flight.";
  } else {
    reserveButtonTemplate = reserveButtonTemplate.replace(
      'disabled="disabled"',
      ""
    );
  }
});

function parseDate(date) {
  // 2021-04-25T08:35:00
  var parts = date.split("T");
  // 2021-04-25
  var dateParts = parts[0].split("-");
  var time = parts[1];
  return (
    dateParts[1] +
    "/" +
    dateParts[2] +
    "/" +
    dateParts[0] +
    " " +
    time.substr(0, 5)
  );
}

function parseDateAjax(date) {
  // 04/25/2021
  var dateParts = date.split("/");
  return dateParts[2] + "-" + dateParts[0] + "-" + dateParts[1];
}

var reserveButtonTemplate =
  '<button disabled="disabled" type="button" onclick="reserveClick()" class="pure-button button-reserve">Reserve</button>';

var postSabreDone, postAmadeusDone;

function searchClick() {
  event.preventDefault();

  var startDate = document.getElementById("start-date").value;
  var endDate = document.getElementById("end-date").value;
  var origin = selectpureOrigin.value();
  var destination = selectpureDestination.value();
  var passengers = document.getElementById("passengers").value;

  var elemMsg = document.getElementById("search-msg");
  var elemMsgWrapper = document.getElementById("search-msg-warapper");
  var errors = [];
  if (!startDate) errors.push("• Start date field is required");
  //if (!endDate) errors.push("• Return date field is required");
  if (!origin) errors.push("• Origin field is required");
  if (!destination) errors.push("• Destination field is required");
  if (!passengers) errors.push("• Passengers field is required");
  if (errors.length) {
    elemMsg.innerHTML = errors.join("<br />");
    elemMsg.style = "color: #ff3a3a";
    elemMsgWrapper.style = "display: initial";
    return;
  } else {
    elemMsg.innerHTML = "";
    elemMsgWrapper.style = "display: none";
  }

  var searchButtonEle = document.getElementById("search-button");
  var loadingEle = document.getElementById("results-loading");
  var resultsMsgEle = document.getElementById("results-msg");
  var table = document.getElementById("search-results").tBodies[0];
  searchButtonEle.setAttribute("disabled", "disabled");
  loadingEle.style = "";
  resultsMsgEle.style = "display: none";
  resultsMsgEle.innerHTML = "";
  table.innerHTML = "";

  var postData = {
    originLocationCode: origin,
    departureDate: parseDateAjax(startDate),
    adults: passengers
  };
  if (destination) postData.destinationLocationCode = destination;

  postAmadeusDone = false;
  postSabreDone = false;

  postAmadeus(postData);
  postSabre(postData);
}

function postAmadeus(postData) {
  var postUrl =
    "https://kcst84wqtk.execute-api.us-east-1.amazonaws.com/dev/amadeus";

  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", postUrl, true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      console.log("Request response:", this.status);
      console.log(this.responseText);

      var resultsMsgEle = document.getElementById("results-msg");

      if (this.status == 200) {
        var responseJson = JSON.parse(this.responseText);
        if (Object.keys(responseJson).length) {
          printResults(responseJson);
        } else {
          resultsMsgEle.style = "";
          resultsMsgEle.innerHTML += "No results from Amadeus<br />";
        }
      } else {
        resultsMsgEle.style = "color: #ff3a3a";
        resultsMsgEle.innerHTML +=
          "There was an error with Amadeus search<br />";
      }
      postAmadeusDone = true;
      if (postSabreDone) postsAllDone();
    }
  };
  xhttp.send(JSON.stringify(postData));
}

function postSabre(postData) {
  var postUrl =
    "https://kcst84wqtk.execute-api.us-east-1.amazonaws.com/dev/sabre";

  // sabre needs adults to be of type integer
  postData.adults = parseInt(postData.adults);

  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", postUrl, true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      console.log("Request response:", this.status);
      console.log(this.responseText);

      var resultsMsgEle = document.getElementById("results-msg");

      if (this.status == 200) {
        var responseJson = JSON.parse(this.responseText);
        if (responseJson.tickets && responseJson.tickets.length) {
          // modify sabre response so it looks like amadeus
          var responseJsonAdapted = {};

          var nextKey = 100;
          for (var i = 0; i < responseJson.tickets.length; i++) {
            var item = responseJson.tickets[i];

            var data = {};
            // amadeus: 2021-04-25T08:35:00
            // sabre: 12:23:34+01:23
            data.departure_time =
              item.departureDate + "T" + item.departure.time.substr(0, 8);
            // there is no arrival date in sabre response...
            // so we add one day to departure time if the time of arrival is before
            if (
              parseInt(item.arrival.time.substr(0, 2)) <
              parseInt(item.departure.time.substr(0, 2))
            ) {
              var date = new Date(item.departureDate);
              date.setDate(date.getDate() + 1);
              data.arrival_time =
                date.toISOString().substr(0, 10) +
                "T" +
                item.arrival.time.substr(0, 8);
            } else
              data.arrival_time =
                item.departureDate + "T" + item.arrival.time.substr(0, 8);
            data.price = {total: item.totalPrice};
            data.isSabre = true;

            responseJsonAdapted[nextKey++] = data;
          }

          printResults(responseJsonAdapted);
        } else {
          resultsMsgEle.style = "";
          resultsMsgEle.innerHTML += "No results from Sabre<br />";
        }
      } else {
        resultsMsgEle.style = "color: #ff3a3a";
        resultsMsgEle.innerHTML += "There was an error with Sabre search<br />";
      }
      postSabreDone = true;
      if (postAmadeusDone) postsAllDone();
    }
  };
  xhttp.send(JSON.stringify(postData));
}

function postsAllDone() {
  var searchButtonEle = document.getElementById("search-button");
  var loadingEle = document.getElementById("results-loading");

  searchButtonEle.removeAttribute("disabled");
  loadingEle.style = "display:none";

  postAmadeusDone = false;
  postSabreDone = false;
}

function printResults(results) {
  var table = document.getElementById("search-results").tBodies[0];
  var rows = 0;

  var origin = "",
    origin_label = "";
  if (selectpureOrigin._selectedOption) {
    origin = selectpureOrigin.value();
    origin_label = selectpureOrigin._selectedOption.label;
  }

  var destination = "",
    destination_label = "";
  if (selectpureDestination._selectedOption) {
    destination = selectpureDestination.value();
    destination_label = selectpureDestination._selectedOption.label;
  }

  var passengers = document.getElementById("passengers").value || "";

  for (var key in results) {
    var data = results[key];

    var departure_time = data.departure_time;
    var arrival_time = data.arrival_time;
    var price = data.price.total;
    var isSabre = data.isSabre;

    var row = table.insertRow(-1);
    rows++;
    if (rows % 2 == 1) row.className = "pure-table-odd";

    row.dataset.tripId = Math.random()
      .toString()
      .substr(2, 8);
    row.dataset.userId = localStorage.email;

    row.dataset.origin = origin;
    row.dataset.origin_label = origin_label;
    row.dataset.destination = destination;
    row.dataset.destination_label = destination_label;
    row.dataset.passengers = passengers;
    row.dataset.departure_time = departure_time;
    row.dataset.arrival_time = arrival_time;
    row.dataset.price = price;

    var cell;

    // #
    cell = row.insertCell(-1);
    cell.innerHTML = rows;

    // departure time
    cell = row.insertCell(-1);
    cell.innerHTML = parseDate(departure_time);

    // arrival time
    cell = row.insertCell(-1);
    cell.innerHTML = parseDate(arrival_time);

    // price
    cell = row.insertCell(-1);
    cell.innerHTML = "$" + price;

    // source
    cell = row.insertCell(-1);
    // vars defined in search.html
    var logoImgPath = isSabre ? sabreLogoPath : amadeusLogoPath;
    var logoImg =
      "<img class='source-logo' src='" + logoImgPath + "' width='70px' />";
    cell.innerHTML = logoImg;

    // reserve button
    cell = row.insertCell(-1);
    cell.innerHTML = reserveButtonTemplate;
  }
}

function reserveClick() {
  var button = event.target;
  var row = button.parentElement.parentElement;
  var data = row.dataset;
  console.log("Ticket data:", data);

  var loadingEle = document
    .getElementById("reserve-loading-template")
    .cloneNode(true);
  loadingEle.id = "";
  loadingEle.style = "";
  button.parentElement.appendChild(loadingEle);

  var reserveInputData = {
    user_id: data.userId,
    flight_id: data.tripId,
    arrival_iata_code: data.destination,
    arrival_time: data.arrival_time,
    departure_iata_code: data.origin,
    departure_time: data.departure_time,
    price_currency: "EUR",
    price_total: data.price,
    itineraries_duration: "PT00H",
    itineraries_segments: "0"
  };

  var reservePostData = {
    stateMachineArn:
      "arn:aws:states:us-east-1:077183341186:stateMachine:HelloWorld",
    input: JSON.stringify(reserveInputData)
  };

  var reservePostUrl =
    "https://t5ppbjldsl.execute-api.us-east-1.amazonaws.com/alpha/execution";

  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", reservePostUrl, true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      console.log("Request response:", this.status, this.responseText);

      if (this.status == 200)
        pollReserveStatus(data.userId, data.tripId, function(
          pollErrMsg,
          pollOkMsg
        ) {
          loadingEle.parentElement.removeChild(loadingEle);

          console.log("Poll reserve returned:", pollErrMsg, pollOkMsg);

          alert(pollErrMsg || pollOkMsg);

          if (!pollErrMsg) openPaymentTab(data);
        });
      else {
        loadingEle.parentElement.removeChild(loadingEle);
        alert("There was an error. Please try again");
      }
    }
  };
  xhttp.send(JSON.stringify(reservePostData));
}

function openPaymentTab(data) {
  var queryArgs = {
    userId: data.userId,
    tripId: data.tripId,
    origin: data.origin,
    origin_label: data.origin_label,
    destination: data.destination,
    destination_label: data.destination_label,
    departure_time: data.departure_time,
    arrival_time: data.arrival_time,
    passengers: data.passengers,
    price: data.price
  };

  var queryArr = [];
  for (var key in queryArgs)
    queryArr.push(key + "=" + encodeURIComponent(queryArgs[key]));
  var queryStr = queryArr.join("&");

  var url = location.href.replace("search", "payment") + "?" + queryStr;
  window.open(url, "_blank");
}
