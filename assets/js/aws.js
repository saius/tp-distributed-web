var dynamoDocClient;

var ERROR_RESERVE_CANCELED = 100;

window.addEventListener("load", function() {
  var aws_region = "us-east-1";
  var aws_identity_pool_id = "us-east-1:fa74b6cd-c7b9-43d2-9179-71c1c6f2faae";

  console.log(
    "Using AWS region:",
    aws_region,
    "|",
    "identity pool id:",
    aws_identity_pool_id
  );

  AWS.config.update({
    region: aws_region,
    // Initialize the Amazon Cognito credentials provider
    credentials: new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_identity_pool_id
    })
  });

  dynamoDocClient = new AWS.DynamoDB.DocumentClient();
});

function queryBookingStatus(userId, flightId, queryCallback) {
  var tableName = "booking_status";

  // https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.NodeJs.04.html
  var params = {
    TableName: tableName,
    KeyConditionExpression: "user_id = :userId and flight_id = :flightId",
    ExpressionAttributeValues: {
      ":userId": userId,
      ":flightId": flightId
    }
  };

  console.log(
    "Querying table",
    tableName,
    " | userId:",
    userId,
    " | flight_id:",
    flightId,
    "..."
  );

  dynamoDocClient.query(params, function(err, data) {
    if (err) {
      var errorMsg = JSON.stringify(err, null, 2);
      console.error("Unable to query. Error:", errorMsg);
      queryCallback(errorMsg, null);
    } else {
      console.log("Query succeeded. Data:", data);
      queryCallback(null, data);
    }
  });
}

var pollIntervalMS = 700;

function pollReserveStatus(userId, flightId, pollCallback) {
  function checkReserveStatus(userId, flightId) {
    console.log("Checking reserve status...");
    queryBookingStatus(userId, flightId, function(err, data) {
      if (err) {
        pollCallback(err, null);
      } else {
        if (data && data.Count && data.Items[0].statusReserveOk !== undefined) {
          var bookingStatus = data.Items[0];

          if (bookingStatus.statusReserveOk) {
            // return msg as ok
            pollCallback(
              null,
              bookingStatus.statusReserveMsg || "Reserve done"
            );
          } else {
            // return msg as error
            pollCallback(
              bookingStatus.statusReserveMsg ||
                "There was an error during the reservation",
              null
            );
          }
        } else {
          // if there are no records its because its still processing it
          console.log("Wait reserve poll timeout...");
          setTimeout(function() {
            checkReserveStatus(userId, flightId);
          }, pollIntervalMS);
        }
      }
    });
  }

  checkReserveStatus(userId, flightId);
}

function pollPaymentStatus(userId, flightId, pollCallback) {
  function checkPaymentStatus(userId, flightId) {
    console.log("Checking payment status...");
    queryBookingStatus(userId, flightId, function(err, data) {
      if (err) {
        pollCallback(err, null);
      } else {
        if (data && data.Count) {
          var bookingStatus = data.Items[0];

          if (!bookingStatus.statusReserveOk) {
            pollCallback(ERROR_RESERVE_CANCELED, null);
          } else if (bookingStatus.statusPaymentOk === undefined) {
            // if there is no field its because its still processing it
            console.log("Wait payment poll timeout...");
            setTimeout(function() {
              checkPaymentStatus(userId, flightId);
            }, pollIntervalMS);
          } else if (bookingStatus.statusPaymentOk) {
            // return msg as ok
            pollCallback(null, bookingStatus.statusPaymentMsg);
          } else {
            // return msg as error
            pollCallback(bookingStatus.statusPaymentMsg, null);
          }
        } else {
          // if there is no records there is an error
          pollCallback("No payment found", null);
        }
      }
    });
  }

  checkPaymentStatus(userId, flightId);
}

function pollPurchaseStatus(userId, flightId, pollCallback) {
  function checkPurchaseStatus(userId, flightId) {
    console.log("Checking purchase status...");
    queryBookingStatus(userId, flightId, function(err, data) {
      if (err) {
        pollCallback(err, null);
      } else {
        if (data && data.Count) {
          var bookingStatus = data.Items[0];

          if (!bookingStatus.statusReserveOk) {
            pollCallback(ERROR_RESERVE_CANCELED, null);
          } else if (bookingStatus.statusPurchaseOk === undefined) {
            // if there is no field its because its still processing it
            console.log("Wait purchase poll timeout...");
            setTimeout(function() {
              checkPurchaseStatus(userId, flightId);
            }, pollIntervalMS);
          } else if (bookingStatus.statusPurchaseOk) {
            // return msg as ok
            pollCallback(null, bookingStatus.statusPurchaseMsg);
          } else {
            // return msg as error
            pollCallback(bookingStatus.statusPurchaseMsg, null);
          }
        } else {
          // if there is no records there is an error
          pollCallback("No purchase found", null);
        }
      }
    });
  }

  checkPurchaseStatus(userId, flightId);
}
