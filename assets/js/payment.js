function parseDate(date) {
  // 2021-04-25T08:35:00
  var parts = date.split("T");
  // 2021-04-25
  var dateParts = parts[0].split("-");
  var time = parts[1];
  return (
    dateParts[1] +
    "/" +
    dateParts[2] +
    "/" +
    dateParts[0] +
    " " +
    time.substr(0, 5)
  );
}

var queryData = {};

window.addEventListener("load", function() {
  var queryStr = location.search.substr(1);
  var queryArr = queryStr.split("&");
  for (var i = 0; i < queryArr.length; i++) {
    var paramParts = queryArr[i].split("=");
    queryData[paramParts[0]] = decodeURIComponent(paramParts[1]);
  }

  queryData.userId = localStorage.email;

  console.log("Query data loaded:", queryData);

  document.getElementById("origin").innerHTML = queryData.origin_label;
  document.getElementById("destination").innerHTML =
    queryData.destination_label;
  document.getElementById("departure-date").innerHTML = parseDate(
    queryData.departure_time
  );
  document.getElementById("arrival-date").innerHTML = parseDate(
    queryData.arrival_time
  );
  document.getElementById("passengers").innerHTML = queryData.passengers;
  document.getElementById("price").innerHTML = "$" + queryData.price;
});

function purchaseClick() {
  var cardNumber = document.getElementById("card-number").value;
  var cardName = document.getElementById("card-name").value;
  var cardExpiry = document.getElementById("card-expiry").value;
  var cardCode = document.getElementById("card-code").value;

  var elemMsg = document.getElementById("card-msg");
  var elemMsgWrapper = document.getElementById("card-msg-warapper");
  var errors = [];
  if (!cardNumber) errors.push("• Card number date field is required");
  if (cardNumber.replace(/ /g, "").length < 16)
    errors.push("• Card number must have 16 digits");
  if (!cardName) errors.push("• Name on card field is required");
  if (!cardExpiry) errors.push("• Expiry date field is required");
  if (!cardCode) errors.push("• Security code field is required");
  if (errors.length) {
    elemMsg.innerHTML = errors.join("<br />");
    elemMsg.style = "color: #ff3a3a";
    elemMsgWrapper.style = "display: initial";
    return;
  } else {
    elemMsg.innerHTML = "";
    elemMsgWrapper.style = "display: none";
  }

  var loadingEle = document.getElementById("purchase-loading");
  var loadingMsgEle = document.getElementById("purchase-loading-msg");
  loadingMsgEle.innerHTML = "Sending payment information...";
  loadingEle.style = "";

  var paymentPostData = {
    user_id: queryData.userId,
    flight_id: queryData.tripId,
    cardnumber: cardNumber,
    name: cardName,
    expiry_date: cardExpiry,
    CVV: cardCode
  };

  var paymentPostUrl =
    "https://t5ppbjldsl.execute-api.us-east-1.amazonaws.com/alpha/paymentinfo";

  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", paymentPostUrl, true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      console.log("Request response:", this.status, this.responseText);

      if (this.status == 200) {
        loadingMsgEle.innerHTML = "Processing payment...";
        pollPaymentStatus(queryData.userId, queryData.tripId, function(
          pollErrMsg,
          pollOkMsg
        ) {
          console.log("Poll payment returned:", pollErrMsg, pollOkMsg);

          if (pollErrMsg) {
            loadingEle.style = "display:none";
            loadingMsgEle.innerHTML = "";
            if (pollErrMsg == ERROR_RESERVE_CANCELED) {
              errorLockForm();
              pollErrMsg =
                "The payment was rejected, the reserve has been canceled";
            }
            alert(pollErrMsg);
          } else {
            loadingMsgEle.innerHTML =
              "Payment accepted, processing purchase...";

            pollPurchaseStatus(queryData.userId, queryData.tripId, function(
              pollErrMsg2,
              pollOkMsg2
            ) {
              console.log("Poll purchase returned:", pollErrMsg2, pollOkMsg2);

              loadingEle.style = "display:none";
              loadingMsgEle.innerHTML = "";
              if (pollErrMsg2) {
                if (pollErrMsg2 == ERROR_RESERVE_CANCELED) {
                  errorLockForm();
                  pollErrMsg2 =
                    "The purchase was rejected, the reserve has been canceled";
                }
                alert(pollErrMsg2);
              } else {
                document.getElementById("payment-values").style =
                  "display: none";
                document.getElementById("payment-success").style =
                  "display: initial";
              }
            });
          }
        });
      } else {
        loadingEle.style = "display:none";
        loadingMsgEle.innerHTML = "";
        alert("There was an error. Please try again");
      }
    }
  };
  xhttp.send(JSON.stringify(paymentPostData));
}

function errorLockForm() {
  var eles = [
    document.getElementById("card-number"),
    document.getElementById("card-name"),
    document.getElementById("card-expiry"),
    document.getElementById("card-code"),
    document.getElementById("purchase-button")
  ];

  for (var i = 0; i < eles.length; i++) {
    eles[i].setAttribute("disabled", "disabled");
  }
}
