window.addEventListener("load", function() {
  if (localStorage.loggedin) {
    var signinForm = document.getElementById("singin-form");
    var loggedinForm = document.getElementById("loggedin-form");
    signinForm.style = "display: none";
    loggedinForm.style = "";

    var elemEmail = document.getElementById("loggedin-email");
    elemEmail.innerHTML = localStorage.email;
  }
});

function signInSubmit() {
  event.preventDefault();

  var elemEmail = document.getElementById("signin-email");
  var elemPass = document.getElementById("signin-password");
  var elemMsg = document.getElementById("signin-msg");
  var email = elemEmail.value;
  var pass = elemPass.value;

  if (!email || !pass) {
    elemMsg.innerHTML = "Email and password are required";
    elemMsg.style = "color: #ff3a3a";
  } else if (!localStorage.email || !localStorage.pass) {
    elemMsg.innerHTML = "You must register first";
    elemMsg.style = "color: #ff3a3a";
  } else if (localStorage.email == email && localStorage.pass == pass) {
    localStorage.loggedin = true;
    elemMsg.innerHTML = "Login successful";
    elemMsg.style = "color: #0da80d";
    setTimeout("location.reload()", 1500);
  } else {
    elemMsg.innerHTML = "Invalid credentials";
    elemMsg.style = "color: #ff3a3a";
  }
}

function registerSubmit() {
  event.preventDefault();

  var elemEmail = document.getElementById("register-email");
  var elemPass = document.getElementById("register-password");
  var elemMsg = document.getElementById("register-msg");
  var email = elemEmail.value;
  var pass = elemPass.value;

  if (!email || !pass) {
    elemMsg.innerHTML = "Email and password are required";
    elemMsg.style = "color: #ff3a3a";
  } else {
    if (localStorage.email || localStorage.pass) {
      if (
        !confirm(
          "Only one user can be registered.\nThis will overwrite the previous one.\nAre you sure you want to continue?"
        )
      ) {
        elemMsg.innerHTML = "";
        elemMsg.style = "";
        return;
      }
    }
    localStorage.email = email;
    localStorage.pass = pass;
    elemMsg.innerHTML = "Registration successful, you can now login";
    elemMsg.style = "color: #0da80d";
    setTimeout("location.reload()", 1500);
  }
}

function logOutSubmit() {
  event.preventDefault();
  if (confirm("Are you sure you want to log out?")) {
    delete localStorage.loggedin;
    location.reload();
  }
}
